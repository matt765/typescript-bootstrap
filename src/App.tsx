import EmailVerifier from "./Components/EmailVerifier/index";
import UsersList from "./Components/UsersList";
import ColorPicker from "./Components/ColorPicker";
import { Link, Routes, Route, BrowserRouter as Router } from "react-router-dom";
import Logo from "./Assets/Images/tslogo.png";
import { GitlabOutlined } from "@ant-design/icons";
const App = () => {
  return (
    <Router>
      <div className="app-container d-flex justify-content-center align-items-center">
        <div className="app d-flex align-items-center flex-column"  id="app">
          <div className="header d-flex align-items-center">
            <div className="header-logo d-flex align-items-center">
              <div className="header-logo-image">
                <img src={Logo} />
              </div>
              <div className="header-logo-text d-flex">
                <div className="header-logo-text-first">TypeScript</div>
                <div className="header-logo-text-second">Apps</div>
              </div>
            </div>
            <div className="header-links d-flex justify-content-end">
              <Link to="/emailverifier">E-mail Verifier</Link>
              <Link to="/colorpicker">Color picker</Link>
              <Link to="/userslist">List of users</Link>
            </div>
            <div className="header-gitlab">
              <a
                className="header-gitlab-icon"
                href="https://gitlab.com/matt765/typescript-bootstrap"
                target="_blank"
              >
                <GitlabOutlined />
              </a>
            </div>
          </div>

          <div className="app-routes d-flex justify-content-center">
            <Routes>
              <Route path="/" element={<EmailVerifier />} />
              <Route path="/emailverifier" element={<EmailVerifier />} />
              <Route path="/colorpicker" element={<ColorPicker />} />
              <Route path="/userslist" element={<UsersList />} />
            </Routes>
          </div>
        </div>
      </div>
    </Router>
  );
};

export default App;
