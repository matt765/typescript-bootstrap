function checkEmail(
  setValidationResult: (result: string) => void,
  setLoading: (loading: boolean) => void,
  address: string
) {
  const corsProxy = "https://mycorsproxy765.herokuapp.com/";
  const apikey = process.env.REACT_APP_EMAIL_VERIFIER_API_KEY;
  const endpoint = `https://api.email-validator.net/api/verify?EmailAddress=${address}&APIKey=${apikey}`;
 
  setLoading(true);

  fetch(`${corsProxy}${endpoint}`, {
    method: "GET",
  })
    .then(function (response) {
      if (response.status !== 200) {
        console.log(
          "Looks like there was a problem. Status code:" + response.status
        );
        setLoading(false);
        return;
      }
      response.json().then((data) => {
        if (!data.status) {
          setValidationResult(data.error.message);
        } else {
          setValidationResult(data.details);
        }
        setLoading(false);
      });
    })
    .catch(function (err) {
      console.log("Fetch Error", err);
    });
}

export default checkEmail;
