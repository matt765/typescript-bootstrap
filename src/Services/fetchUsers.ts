import axios from "axios";
import { User } from "../Components/UsersList/index";

function fetchUsers(
  setUserData: (result: User[]) => void,
  setLoading: (loading: boolean) => void
) {
  setLoading(true);
  axios
    .get("https://jsonplaceholder.typicode.com/users")
    .then(function (response) {
      setUserData(Object.values(response.data));
      setLoading(false);
    })
    .catch(function (error) {
      console.log(error);
      setLoading(false);
    });
}

export default fetchUsers;
