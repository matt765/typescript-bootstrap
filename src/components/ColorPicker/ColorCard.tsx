import * as React from "react";
import { useState } from "react";

interface Props {
  hexValue: string;
  rgbValue: string;
  setMessage: (message:string) => void
}

const ColorCard = ({ hexValue, rgbValue, setMessage }: Props): JSX.Element => {
  function handleSubmit(event: React.SyntheticEvent) {}

  const styles = {
    colorCardBox: {
      backgroundColor: hexValue,
    },
  } as const;

  function changeBackground() {
    document.getElementById("app")!.style.backgroundColor = hexValue;
  }

  return (
    <>
      <div className="color-card-container d-flex flex-column justify-content-center align-items-center">
        <div
          className="color-card-box"
          style={styles.colorCardBox}
          onClick={changeBackground}
        ></div>
        <div
          className="color-card-value color-card-hex"
          onClick={() => {
            navigator.clipboard.writeText(hexValue);
            setMessage(hexValue);
          }}
        >
          {hexValue}
        </div>
        <div
          className="color-card-value color-card-rgb"
          onClick={() => {
            navigator.clipboard.writeText(rgbValue);
            setMessage(rgbValue);
          }}
        >
          {rgbValue}
        </div>
      </div>
    </>
  );
};

export default ColorCard;
