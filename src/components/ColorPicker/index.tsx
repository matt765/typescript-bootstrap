import { useState } from "react";
import ColorCard from "./ColorCard";

const ColorPicker = (): JSX.Element => {
  const [message, setMessage] = useState("");

  return (
    <>
      <div className="d-flex flex-column">
        <div className="color-picker-message">{message && `Copied: ${message}`}</div>
        <div className="color-picker-cards-container d-flex flex-column">
          <div className="color-picker-cards-first-row d-flex">
            <ColorCard hexValue="#ffffff" rgbValue="rgb(255, 255, 255)" setMessage={setMessage} />
            <ColorCard hexValue="#F4D03F" rgbValue="rgb(244, 208, 63)" setMessage={setMessage} />
            <ColorCard hexValue="#58D68D" rgbValue="rgb(88, 214, 141)" setMessage={setMessage} />
            <ColorCard hexValue="#76D7C4" rgbValue="rgb(118, 215, 196 )" setMessage={setMessage} />
            <ColorCard hexValue="#85C1E9" rgbValue="rgb(133, 193, 233)" setMessage={setMessage} />
          </div>
          <div className="color-picker-cards-second-row d-flex">
            <ColorCard hexValue="#BB8FCE" rgbValue="rgb(187, 143, 206)" setMessage={setMessage} />
            <ColorCard hexValue="#E74C3C" rgbValue="rgb(231, 76, 60)" setMessage={setMessage} />
            <ColorCard hexValue="#2E4053" rgbValue="rgb(46, 64, 83)" setMessage={setMessage} />
            <ColorCard hexValue="#88a2c8" rgbValue="rgb(136, 162, 200)" setMessage={setMessage} />
            <ColorCard hexValue="#5D6D7E " rgbValue="rgb(93, 109, 126)" setMessage={setMessage} />
          </div>
        </div>
      </div>
    </>
  );
};

export default ColorPicker;
