import * as React from "react";
import { useState } from "react";
import checkEmail from "../../Services/checkEmail";

interface Props {
  setValidationResult: (result: string) => void;
  setLoading: (loading: boolean) => void; 
}

const Form = ({ setValidationResult, setLoading }: Props): JSX.Element => {
  const [address, setAddress] = useState("");

  function handleSubmit(event: React.SyntheticEvent) {
    event.preventDefault();
    if (address) {
      checkEmail(setValidationResult, setLoading, address);
    } else {
      setValidationResult("You forgot to write the adress mate");
    }
  }

  return (
    <>
      <form onSubmit={handleSubmit} className="email-verifier-form">
        <input type="text" className="app-content-input" onChange={(e) => setAddress(e.target.value)} />
        <button className="app-content-button">Check E-mail</button>
      </form>
    </>
  );
};

export default Form;
