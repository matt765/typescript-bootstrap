import { LoadingOutlined } from "@ant-design/icons";

interface Props {
  validationResult: string;
  loading: boolean;
}

const Result = ({ validationResult, loading }: Props): JSX.Element => {
  return (
    <>
      <div className="email-verifier-result">
        {!loading ? validationResult : <LoadingOutlined />}
      </div>
    </>
  );
};

export default Result;
