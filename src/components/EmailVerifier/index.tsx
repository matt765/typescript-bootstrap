import Form from "./Form";
import Result from "./Result";
import { useState } from "react";

const EmailVerifier = (): JSX.Element => {
  const [validationResult, setValidationResult] = useState("");
  const [loading, setLoading] = useState(false);

  return (
    <>
      <div className="d-flex flex-column align-items-center justify-content-center mb-5">
        <Form
          setValidationResult={setValidationResult}
          setLoading={setLoading}
        />
        <Result validationResult={validationResult} loading={loading} />
      </div>
    </>
  );
};

export default EmailVerifier;
