import { User } from "./index";
import fetchUsers from "../../Services/fetchUsers";

interface Props {
  setUserData: (result: User[]) => void;
  setLoading: (loading: boolean) => void; 
}

const Button = ({ setUserData, setLoading }: Props): JSX.Element => {

  function clickHandler() {
    fetchUsers(setUserData, setLoading);
  }

  return (
    <>
      <button className="app-content-button" onClick={clickHandler}>
        Fetch users
      </button>
    </>
  );
};

export default Button;
