import * as React from "react";
import { User } from "./index";
import { LoadingOutlined } from "@ant-design/icons";

interface Props {
  userData: User[];
  loading: boolean;
}

const Result = ({ userData, loading }: Props): JSX.Element => {
  React.useEffect(() => {
    if (userData) {
      renderData();
    }
  }, [userData]);

  function renderData() {
    if (userData.length > 1) {
      return (
        <table className="table table-light ">
          <tbody>
            <tr>
              <th>id</th>
              <th>name</th>
              <th>e-mail</th>
            </tr>
            {userData.map((e) => {
              return (
                <tr key={e.name}>
                  <td>{e.id}</td>
                  <td>{e.name}</td>
                  <td>{e.email}</td>
                </tr>
              );
            })}
          </tbody>
        </table>
      );
    }
  }

  return (
    <>
      <div className="mt-2">{loading ? <LoadingOutlined /> : renderData()}</div>
    </>
  );
};

export default Result;
