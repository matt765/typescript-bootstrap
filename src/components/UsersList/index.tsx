import Button from "./Button";
import Result from "./Result";
import { useState } from "react";

export interface User {
  id: number;
  name: string;
  email: string;
}

const UsersList = (): JSX.Element => {
  const [userData, setUserData] = useState<User[]>([]);
  const [loading, setLoading] = useState(false);

  return (
    <>
      <div className="userslist w-100 d-flex align-items-center flex-column">
        <Button setUserData={setUserData} setLoading={setLoading} />
        <Result userData={userData} loading={loading} />
      </div>
    </>
  );
};

export default UsersList;
